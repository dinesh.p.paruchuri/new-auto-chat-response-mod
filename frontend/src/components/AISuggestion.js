import React from "react";
import soundwave from "../assets/images/soundwave.svg";
import send from "../assets/images/send.svg";
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import SwipeUpAltIcon from '@mui/icons-material/SwipeUpAlt';
import FindReplaceIcon from '@mui/icons-material/FindReplace';
import SwipeDownAltIcon from '@mui/icons-material/SwipeDownAlt';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import ViewHeadlineIcon from '@mui/icons-material/ViewHeadline';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

const AISuggestion = ({ suggestion, sendChat, reGenerate, loading }) => {
  return (
    <>
      {/* <div className="ai-title">
        <img src={soundwave} alt="" />
        <h3 className="mb-0">AI Suggested Responses</h3>
      </div> */}
      {loading ? (
        <div className="chat-hldr">
          <h5>Please Wait...</h5>
        </div>
      ) : (
        <>
          {suggestion &&
            suggestion?.map((val, i) => {
              return (
                <div className="respo-hldr" key={i}>
                  <div
                    className="respo-chat"
                    onClick={() => sendChat(val?.response, "admin", "John Doe")}
                    style={{ cursor: "pointer" }}
                  >
                    <p>{val?.response}</p>
                  </div>
                </div>
              );
            })}
        </>
      )}

      <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-evenly" }}>
        <div style={{ height: "27px", width: "100px", border: "1px solid red", borderRadius: "15px", marginLeft: "-130px", padding: "3px" }}>
          <SwipeUpAltIcon style={{ marginTop: "-5px", color: "#EE6B6B" }} /><span style={{ fontSize: "15px", color: "#EE6B6B" }}>She/Her</span>
        </div>
        <div style={{ height: "27px", width: "100px", background: "orange", border: "1px solid orange", borderRadius: "15px", marginLeft: "-110px", padding: "3px" }}>
          <SwipeDownAltIcon style={{ marginTop: "-5px", color: "white" }} /><span style={{ fontSize: "15px", color: "white" }}>He/Him</span>
        </div>
        <div style={{ height: "27px", width: "125px", border: "1px solid blue", borderRadius: "15px", marginLeft: "-110px", padding: "3px" }}>
          <FindReplaceIcon style={{ marginTop: "-5px", color: "blue" }} /><span style={{ fontSize: "14px", color: "blue" }}>They/Them</span>
        </div>
      </div>
      <br></br>
      <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
        <div style={{ display: "flex", flexDirection: "column", borderColor: "#009AFF", borderStyle: "solid", borderRadius: "10px", height: "135px", marginTop: "10px" }}>
          <div style={{ fontSize: "17px", paddingLeft: "5px", color: "#009AFF",fontStyle:"italic" }}><b>Positive response</b></div>
          <ThumbUpIcon style={{ marginLeft: "400px", marginTop: "70px" }} />
          <ThumbDownIcon style={{ marginLeft: "427px", marginTop: "-21px" }} />
          <div id="Positive"></div>
        </div>
        <br></br>
        <div style={{ display: "flex", flexDirection: "column", borderColor: "#64BDFD", borderStyle: "solid", borderRadius: "10px", height: "135px", marginTop: "10px" }}>
          <div style={{ fontSize: "17px", paddingLeft: "5px", color: "#64BDFD",fontStyle:"italic" }}><b>Neutral response</b></div>
          <ThumbUpIcon style={{ marginLeft: "406px", marginTop: "71px" }} />
          <ThumbDownIcon style={{ marginLeft: "433px", marginTop: "-22px" }} />
          <div id="Neutral"></div>
        </div>
        <br></br>
        <br></br>
        <span><LocalOfferIcon style={{ backgroundColor: "black", color: "white", borderRadius: "50%", border: "3px solid black" }} /><b style={{ marginLeft: "12px" }}>Products and Offers</b></span>
        <br></br>
        <span style={{ marginLeft: "11px", marginTop: "-8px" }}><ViewHeadlineIcon /><b style={{ marginLeft: "8px" }}>Atlas travel premium
          <span style={{ marginLeft: "50px" }}><AttachMoneyIcon /><span style={{ marginLeft: "4px" }}>234.00</span>
            <button style={{ width:"100px",marginLeft: "41px", backgroundColor: "#3ACEBE", border: "1px white solid", color: "white", borderRadius: "6px" }}>Suggest</button></span>
          <br></br><span style={{ marginLeft: "32px" }}>Insurance
          </span></b></span>
        <br></br>

        <span style={{ marginLeft: "11px", marginTop: "-8px" }}><ViewHeadlineIcon /><b style={{ marginLeft: "8px" }}>Atlas travel Insurance
          <span style={{ marginLeft: "41px" }}><AttachMoneyIcon /><span style={{ marginLeft: "4px" }}>160.00</span></span></b>

          <button style={{  width:"100px",marginLeft: "41px", backgroundColor: "#3ACEBE", border: "1px white solid", color: "white", borderRadius: "6px" }}>Suggest</button></span>
      </div>






      {/* <button
        className="chat-end-btn"
        onClick={() => {
          reGenerate();
        }}
      >
        <img src={send} alt="End Call" /> Regenerate
      </button> */}
      {/* <div className="msg-box-container">
        <div className="ai-title">
          <img src={soundwave} alt="" />
          <h3 className="mb-0">AI Suggested Policys</h3>
        </div>
        <div>
          <table className="table table-borderless ai-suggest w-100">
            <tbody>
              <tr>
                <td>
                  <img src={award} alt="" />
                </td>
                <td>32.6%</td>
                <td>₹ 1,234.00</td>
                <td>
                  <button>Suggest</button>
                </td>
              </tr>
              <tr>
                <td>
                  <img src={award} alt="" />
                </td>
                <td>32.6%</td>
                <td>₹ 1,234.00</td>
                <td>
                  <button>Suggest</button>
                </td>
              </tr>
              <tr>
                <td>
                  <img src={award} alt="" />
                </td>
                <td>32.6%</td>
                <td>₹ 1,234.00</td>
                <td>
                  <button>Suggest</button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div> */}
    </>
  );
};

export default AISuggestion;
