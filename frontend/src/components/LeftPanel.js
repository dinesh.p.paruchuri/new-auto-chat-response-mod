import React, { useState } from "react";
import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import Redux from "../pages/Redux";
import ApiCall from "../pages/ApiCall";
import { site_text } from "../utils/languageMapper";
import { useDispatch, useSelector } from "react-redux";
import { updateLanguage } from "../redux/slices/config/configSlice";
import pwcwhite from "../assets/images/pwcwhite.png";
import HomeIcon from "@mui/icons-material/Home";
import PasteIcon from '@mui/icons-material/ContentPaste';
import PersonIcon from '@mui/icons-material/Person';
import { size } from "lodash";
import LogoutIcon from '@mui/icons-material/Logout';
export default function LeftPanel() {

    const [showContacts, setShowContacts] = useState(false);

    const config = useSelector((state) => state.config);
    const dispatch = useDispatch();

    window.site_lang = config?.language;
    window.site_text = site_text;

    React.useEffect(() => {
        const lang_value = localStorage.getItem("site-lang");
        if (lang_value) {
            dispatch(updateLanguage(lang_value));
        } else {
            dispatch(updateLanguage("English"));
        }
    }, []);

    const changeLang = (lang) => {
        dispatch(updateLanguage(lang));
        localStorage.setItem("site-lang", lang);
    };
    return (
        <div>
            <div className="main-div" style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                <div className="l-panel d-flex justify-content-start">
                    <div className="logo">
                        <img src={pwcwhite} alt="" />
                    </div>
                    <ul className="page-nav w-100">
                        <li className="active">
                            <HomeIcon />
                        </li>
                        <li className="active" style={{ marginTop: "10px" }}>
                            <PasteIcon />
                        </li>
                        <li className="active" style={{ marginTop: "10px" }}>
                            <PasteIcon />
                        </li>
                        <li className="active" style={{ marginTop: "10px" }}>
                            <PasteIcon />
                        </li>
                        <li className="active" style={{ marginTop: "10px" }}>
                            <PasteIcon />
                        </li>
                        <li className="active" style={{ marginTop: "10px" }}>
                            <PersonIcon />
                        </li>
                        <li className="active" style={{ marginTop: "25px" }}>
                            <LogoutIcon />
                        </li>
                    </ul>
                </div>

            </div>
        </div>

    );

}