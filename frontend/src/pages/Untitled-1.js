<>
<LeftPanel style={{width:'10%'}}></LeftPanel>
    <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between" , width:"90vw", height:"100vh"}}>

      <div className="" style={{ display: "flex", flexDirection: "column",justifyContent:'space-between', position: "relative", maxWidth:"50%" , overflow:'hidden'}}>
        <div style={{ position: "fixed", zIndex: 10 }}>
          <TopBanner />
        </div>
        <div className="px-5 container-fluid r-panel" style={{ position: "relative", zIndex: 1, marginTop: "40px" }}>
          <div className="gap-3 mb-3 d-flex justify-content-between w-100" style={{ paddingLeft: "100px", position: "relative" }}>
            <div className="w-40 bdr-hldr bdr-primary">
                <TabHeader />
                <div className="tab-content" id="myTabContent">
                  {chat && <Chat chat={chat} sendChat={sendChat} loading={loading} />}
                  <Summary />
                </div>
              </div>
            
            <div className="bdr-hldr bdr-primary w-30 agnt-resp">
              <h2>Agent Responses</h2>
            {/* <AISuggestion
                suggestion={suggestion}
                sendChat={sendChat}
                reGenerate={getSuggestion}
                loading={loading}
              /> *
            {/* </div> */}
            <div style={{ display: "flex", flexDirection: "column" , justifyContent:'space-between'}}>
              <SentimentChart NoofSegments={5} stntiment={stntiment} />
              <IntentCharts NoofSegments={5} value={700} />
            </div>
          </div>
        </div>

        <div>
          <CustomerDetails />
        </div>

      </div>

      * Module 2

      
      
      

       {/* Module 3 */}

       
      
</div>

    </div>
    </>